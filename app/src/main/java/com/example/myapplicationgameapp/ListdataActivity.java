package com.example.myapplicationgameapp;

import android.database.Cursor;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;



public class ListdataActivity extends AppCompatActivity{

    private static final String TAG ="ListdataActivity";
    DatabaseHelper mDatabaseHelper;
    private ListView mListView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);
        mListView =(ListView)findViewById(R.id.listView);
        mDatabaseHelper =new DatabaseHelper();

        populateListView();
    }
    private void populateListView(){
        Log.d(TAG,"populateListView: Displaying data in the ListView,");

        Cursor data =mDatabaseHelper.insertData();
        ArrayList<String>listData=new ArrayList();
        while(data.moveToNext()){
            listData.add(data.getString(1));
        }
        ListAdapter adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,listData);
        mListView.setAdapter(adapter);
    }
    private void toastMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
